# movi shop #

**To run application:**

* Build docker images
`sbt phone/docker:publishLocal`
`sbt order/docker:publishLocal`

* Run docker-compose
`docker-compose up`

* connect to postgres `localhost:5432`(default password `aaa123`) and initialize table with following commands:
```
CREATE TABLE IF NOT EXISTS phones (
	id SERIAL,
	name varchar(100),
	description varchar(1000),
	imageUrl varchar(1000),
	price decimal
);
```

```
insert into phones(name, description, imageUrl, price) VALUES
	('Nokia 3300', 'best seller', 'nokia33.jpg', 52.45),
	('Nokia 6200', 'not the worst one', 'nokia6200.jpeg', 33.33),
	('jPhoneXXX', 'do not waste your money', 'jphxxx.jpeg', 4520000.75);
```

* Send post request to order service

```curl --header "Content-Type: application/json" \
   --request POST \
   --data '{"customer":{"name":"x","surname":"y","email":"z"},"phones":[{"id":1,"name":"Nokia 3300","desc":"best seller","imageUrl":"nokia33.jpg","price":52.45},{"id":2,"name":"Nokia 6200","desc":"not the worst one","imageUrl":"nokia6200.jpeg","price":33.33}]}' \
   http://localhost:8081/order
```   
   
---------------------------------

**How would you improve the system?**
1. use non blocking database driver

2. add directives which should validate input parameters

3. single configuration between docker-compose and application.conf

4. add dummy service implementation for integration tests

5. add integration\functional tests

6. add service discovery(zookeeper, consul)


**How would you avoid your order api to be overflow?**

1. since services are stateless they can be scaled easily - just deploy one more if needed

2. add database sharding

3. depending on requirements database can be changed to proper NoSql solution
