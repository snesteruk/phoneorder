lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "com.company",
      scalaVersion    := "2.12.5"
    )),
    name := "movi"
  ).aggregate(phone, order)



lazy val phone = (project in file("service/phone")).
  settings(
    inThisBuild(List(
      organization    := "com.company",
      scalaVersion    := "2.12.5"
    )),
    name := "movi-phone")

lazy val order = (project in file("service/order")).
  settings(
    inThisBuild(List(
      organization    := "com.company",
      scalaVersion    := "2.12.5"
    )),
    name := "movi-order")
