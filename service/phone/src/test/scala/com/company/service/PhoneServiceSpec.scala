package com.company.service

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Seconds, Span}
import scalikejdbc._
import scalikejdbc.config.DBs

import scala.concurrent.ExecutionContext

class PhoneServiceSpec extends FlatSpec with ScalaFutures with Matchers {

  initDB()

  val phoneService = new PhoneServiceImpl(ExecutionContext.global)

  it should "return phones from db" in {
    val ids = List(1, 2)
    val res = phoneService.getPhones(ids)
    whenReady(res) { phones =>
      phones.size === ids.size
      ids.forall(id => phones.map(_.id).contains(id)) should be (true)
    }
  }

  it should "return empty list if id is not present" in {
    val res = phoneService.getPhones(List(555555))
    whenReady(res) { x =>
      x shouldBe empty
    }
  }

  it should "return phones even if some id is not present" in {
    val correctIds = List(1, 2)
    val invalidId = 55555
    val res = phoneService.getPhones(invalidId :: correctIds)
    whenReady(res) { phones =>
      phones.size === correctIds.size
      correctIds.forall(id => phones.map(_.id).contains(id)) should be (true)
    }
  }

  private def initDB(): Unit = {
    DBs.setup()
    DBs.loadGlobalSettings()
    DB autoCommit { implicit session =>
      sql"""CREATE TABLE phones (ID SERIAL, name varchar(100),
      description varchar(1000),
      imageUrl varchar(1000),
      price decimal);""".execute().apply()
    }

    DB localTx { implicit session =>
      sql"insert into phones(name, description, imageUrl, price) VALUES	('phone1', 'phoneDesc', 'url1', 52.45);"
        .update()
        .apply()
      sql"insert into phones(name, description, imageUrl, price) VALUES	('phone2', 'phoneDesc2', 'url2', 11.22);"
        .update()
        .apply()
      sql"insert into phones(name, description, imageUrl, price) VALUES	('phone2', 'phoneDesc2', 'url2', 32.10);"
        .update()
        .apply()
    }

  }

  override implicit def patienceConfig: PatienceConfig = PatienceConfig(timeout = Span(3, Seconds))
}
