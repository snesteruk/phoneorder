package com.company.route

import akka.http.scaladsl.model.{ContentTypes, HttpRequest, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.company.model.{Phone, Phones}
import com.company.service.PhoneService
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FlatSpec, Matchers, OneInstancePerTest}

import scala.concurrent.Future

class PhoneRouteSpec
    extends FlatSpec
    with Matchers
    with ScalaFutures
    with ScalatestRouteTest
    with PhoneRoutes
    with OneInstancePerTest
    with MockFactory {

  val mockedPhoneService = mock[PhoneService]
  override def phoneService: PhoneService = mockedPhoneService

  val phone1 = Phone(1, "p1", "d1", "u1", 10.2)
  val phone2 = Phone(2, "p2", "d2", "u2", 20.3)
  (mockedPhoneService.getPhones _)
    .expects(*)
    .onCall(
      (lst: List[Int]) =>
        lst match {
          case List(2, 1) => Future.successful(List(phone1, phone2))
          case List(322)  => Future.failed(throw new Exception("Boom!"))
          case _          => Future.successful(List())
      }
    )
    .anyNumberOfTimes()

  it should "return phones json if id is correct" in {
    val request = HttpRequest(uri = "/phone/?id=1&id=2")
    request ~> phoneRoutes ~> check {
      status should ===(StatusCodes.OK)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[Phones].items.toSet should ===(Set(phone1, phone2))
    }
  }

  it should "return phones json if id is missing" in {
    val request = HttpRequest(uri = "/phone/?id=1234")
    request ~> phoneRoutes ~> check {
      status should ===(StatusCodes.OK)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[Phones].items.toSet shouldBe empty
    }
  }

  it should "return Internal error if something bad is happened" in {
    val request = HttpRequest(uri = "/phone/?id=322")
    request ~> phoneRoutes ~> check {
      status should ===(StatusCodes.InternalServerError)
    }
  }

}
