CREATE TABLE IF NOT EXISTS phones (
	id SERIAL,
	name varchar(100),
	description varchar(1000),
	imageUrl varchar(1000),
	price decimal
);

insert into phones(name, description, imageUrl, price) VALUES
	('Nokia 3300', 'best seller', 'nokia33.jpg', 52.45),
	('Nokia 6200', 'not the worst one', 'nokia 6200', 33.33),
	('jPhoneXXX', 'do not waste your money', 'jphxxx.jpeg', 4520000.75);
