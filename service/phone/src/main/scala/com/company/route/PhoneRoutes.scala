package com.company.route

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.company.model.Phones
import com.company.service.PhoneService
import com.company.util.PhoneJsonSupport
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait PhoneRoutes extends PhoneJsonSupport with StrictLogging {

  implicit def system: ActorSystem

  def phoneService: PhoneService

  lazy val phoneRoutes: Route =
  pathPrefix("phone") {
    parameters('id.as[Int].*) {ids =>
      val phones = phoneService.getPhones(ids.toList).map(Phones)(ExecutionContext.global)
      onComplete(phones) {
        case Success(p) => complete(p)
        case Failure(ex) =>
          logger.error("Failed to get phones from db", ex)
          complete(StatusCodes.InternalServerError)
      }
    }

  }

}
