package com.company

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.company.route.PhoneRoutes
import com.company.service.PhoneServiceImpl
import com.typesafe.scalalogging.StrictLogging
import scalikejdbc.config.DBs

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

object PhoneServer extends App with PhoneRoutes with StrictLogging {

  implicit val system: ActorSystem = ActorSystem("helloAkkaHttpServer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  DBs.setup()
  DBs.loadGlobalSettings()

  //enough for demo, but separate pool should be used for production
  val dbExecContext = ExecutionContext.global

  val phoneService = new PhoneServiceImpl(dbExecContext)

  lazy val routes: Route = phoneRoutes

  val binding = Http().bindAndHandle(routes, "0.0.0.0", 8080)

  logger.info(s"Server online at http://localhost:8080/")

  Await.result(system.whenTerminated, Duration.Inf)
}
