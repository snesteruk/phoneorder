package com.company.util

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.company.model.{Phone, Phones}
import spray.json.DefaultJsonProtocol

trait PhoneJsonSupport extends SprayJsonSupport {
  import DefaultJsonProtocol._

  implicit val phoneJsonFormat = jsonFormat5(Phone)
  implicit val phonesJsonFormat = jsonFormat1(Phones)

}
