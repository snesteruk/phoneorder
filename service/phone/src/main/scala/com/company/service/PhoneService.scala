package com.company.service

import com.company.model.Phone
import scalikejdbc._

import scala.concurrent.{ExecutionContext, Future, blocking}

trait PhoneService {
  def getPhones(ids: List[Int]): Future[List[Phone]]
}

class PhoneServiceImpl(dbExecCtx: ExecutionContext) extends PhoneService {
  override def getPhones(ids: List[Int]): Future[List[Phone]] =
    Future {
      blocking {
        DB readOnly { implicit session =>
          sql"select * from phones where id in (${ids})"
            .map(
              rs =>
                Phone(rs.get("id"),
                  rs.get("name"),
                  rs.get("description"),
                  rs.get("imageUrl"),
                  rs.get("price")))
            .list()
            .apply()
        }
      }
    }(dbExecCtx)
}
