val akkaHttpVersion = "10.1.1"
val akkaVersion    = "2.5.12"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream"          % akkaVersion,

  "org.scalikejdbc" %% "scalikejdbc"       % "3.2.2",
  "org.scalikejdbc" %% "scalikejdbc-config"  % "3.2.2",
  "org.scalikejdbc" %% "scalikejdbc-test"   % "3.2.2"   % Test,
  "com.h2database"  %  "h2"                 % "1.4.197" % Test,
  "org.scalamock" %% "scalamock" % "4.1.0" % Test,

  "org.postgresql" % "postgresql" % "42.2.2",
  "ch.qos.logback"  %  "logback-classic"   % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",

  "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
  "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
  "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
  "org.scalatest"     %% "scalatest"            % "3.0.1"         % Test
)

dockerBaseImage := "openjdk:jre"

mainClass in Compile := Some("com.company.PhoneServer")

enablePlugins(JavaAppPackaging)

enablePlugins(DockerPlugin)