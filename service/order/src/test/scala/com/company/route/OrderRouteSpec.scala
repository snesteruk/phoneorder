package com.company.route

import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.company.model._
import com.company.service.OrderService
import com.company.service.OrderServiceImpl.{EmptyOrder, InvalidPhone}
import com.company.util.OrderJsonSupport
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FlatSpec, Matchers, OneInstancePerTest}
import spray.json._

import scala.concurrent.Future

class OrderRouteSpec
  extends FlatSpec
    with Matchers
    with ScalaFutures
    with ScalatestRouteTest
    with OrderRoutes
    with OneInstancePerTest
    with OrderJsonSupport
    with MockFactory {


  val orderServiceMock = mock[OrderService]
  override def orderService: OrderService = orderServiceMock

  val Customer1 = Customer("John", "Golt", "j.g@email.com")
  val Hacker = Customer("Anon", "", "hAkka@p.us.gov")
  val Malthael = Customer("Malthael", "", "")

  (orderServiceMock.createOrder _).expects(*)
      .onCall((order: Order) =>
        order match {
          case o@Order(Customer1, phones) =>
            if (phones.isEmpty) Future.successful(Left(EmptyOrder))
            else Future.successful(Right(OrderResult(o, phones.map(_.price).sum)))
          case Order(Hacker, _) => Future.successful(Left(InvalidPhone))
          case Order(Malthael, _) => Future.failed(new Exception("Dead"))
        }
      ).anyNumberOfTimes()

  it should "return order result if order is valid" in {
    val phone1 = Phone(1, "Nokia", "---", "Uri", 234)
    val phone2 = Phone(2, "M1", "---", "Uri2", 100)
    val order = Order(Customer1, List(phone1, phone2)).toJson
    val orderEntity = HttpEntity(ContentTypes.`application/json`, order.toString().getBytes)
    val request = HttpRequest(method = HttpMethods.POST, uri = "/order", entity = orderEntity)
    request ~> orderRoutes ~> check {
      status should ===(StatusCodes.OK)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[OrderResult].totalPrice ===(phone1.price + phone2.price)
    }
  }

  it should "return BadRequest if order is not valid" in {
    val phone = Phone(321, "Nokia", "---", "Uri", 0.01)
    val order = Order(Hacker, List(phone)).toJson
    val orderEntity = HttpEntity(ContentTypes.`application/json`, order.toString().getBytes)
    val request = HttpRequest(method = HttpMethods.POST, uri = "/order", entity = orderEntity)
    request ~> orderRoutes ~> check {
      status should ===(StatusCodes.BadRequest)
    }
  }

  it should "return BadRequest if order is empty" in {
    val order = Order(Hacker, List()).toJson
    val orderEntity = HttpEntity(ContentTypes.`application/json`, order.toString().getBytes)
    val request = HttpRequest(method = HttpMethods.POST, uri = "/order", entity = orderEntity)
    request ~> orderRoutes ~> check {
      status should ===(StatusCodes.BadRequest)
    }
  }

  it should "return Internal server error if orderService is not working properly" in {
    val phone = Phone(321, "Nokia", "---", "Uri", 0.01)
    val order = Order(Malthael, List(phone)).toJson
    val orderEntity = HttpEntity(ContentTypes.`application/json`, order.toString().getBytes)
    val request = HttpRequest(method = HttpMethods.POST, uri = "/order", entity = orderEntity)
    request ~> orderRoutes ~> check {
      status should ===(StatusCodes.InternalServerError)
    }
  }

}
