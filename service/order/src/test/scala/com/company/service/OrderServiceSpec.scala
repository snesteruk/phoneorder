package com.company.service

import com.company.model.{Customer, Order, Phone}
import com.company.service.OrderServiceImpl.{EmptyOrder, InvalidPhone}
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FlatSpec, Matchers, OneInstancePerTest}

import scala.concurrent.{ExecutionContext, Future}

class OrderServiceSpec extends FlatSpec with ScalaFutures with Matchers with MockFactory with OneInstancePerTest {

  implicit val ec: ExecutionContext = ExecutionContext.global

  val httpService = mock[HttpService]

  val validPhone = Phone(1, "Nokia 3300", "bestseller", "someUri", 200.99)
  val validPhone2 = Phone(2, "Nokia 6200", "bestseller", "someUri", 300.99)
  val invalidPhone = Phone(3000, "iPhoneXXX", "piece of ...", "someUri", 999999.99)

  (httpService.getPhones _)
    .expects(*)
    .onCall(
      (ids: List[Int]) => ids match {
        case _ => Future.successful(List(validPhone, validPhone2))
      }
    ).anyNumberOfTimes()

  val orderService = new OrderServiceImpl(httpService)

  val customer = Customer("Amayak", "Akopyan", "a.a@email.com")

  it should "should return order status if order is valid" in {
    val order1 = orderService.createOrder(Order(customer, List(validPhone)))
    whenReady(order1) {
      case Right(result) =>
        result.order.phones.head should be (validPhone)
        result.totalPrice should be (validPhone.price)
      case Left(_) => failTest
    }

    val order2 = orderService.createOrder(Order(customer, List(validPhone, validPhone2)))
    whenReady(order2) {
      case Right(result) =>
        result.order.phones.toSet should be (Set(validPhone, validPhone2))
        result.totalPrice should be (validPhone.price + validPhone2.price)
      case Left(_) => failTest
    }

  }

  it should "return InvalidPhone error if one of phones is invalid" in {
    val order = orderService.createOrder(Order(customer, List(validPhone, invalidPhone)))
    whenReady(order){
      case Right(_) => failTest
      case Left(InvalidPhone) => //ok
    }
  }


  it should "return EmptyOrder error if there is no phone in order" in {
    val order = orderService.createOrder(Order(customer, List()))
    whenReady(order){
      case Right(_) => failTest
      case Left(EmptyOrder) => //ok
    }
  }

  private def failTest = throw new Exception("failed test")
}
