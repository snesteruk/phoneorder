package com.company.model

final case class Phone(id: Int, name: String, desc: String, imageUrl: String, price: BigDecimal)
final case class Phones(items: Seq[Phone])
