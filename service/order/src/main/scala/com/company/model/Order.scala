package com.company.model

final case class Order(customer: Customer, phones: List[Phone])

final case class Customer(name: String, surname: String, email: String)

final case class OrderResult(order: Order, totalPrice: BigDecimal)