package com.company.util

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.company.model._
import spray.json.DefaultJsonProtocol

trait OrderJsonSupport extends SprayJsonSupport {
  import DefaultJsonProtocol._

  implicit val phoneJsonFormat = jsonFormat5(Phone)
  implicit val phonesJsonFormat = jsonFormat1(Phones)

  implicit val customer = jsonFormat3(Customer)

  implicit val orderJsonFormat = jsonFormat2(Order)

  implicit val orderResJsonFormat = jsonFormat2(OrderResult)

}
