package com.company.service

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.Materializer
import akka.stream.scaladsl.{Keep, Sink}
import com.company.model.{Phone, Phones}
import com.company.util.OrderJsonSupport
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import spray.json._

import scala.concurrent.{ExecutionContext, Future}

trait HttpService {
  def getPhones(ids: List[Int]): Future[List[Phone]]
}

class HttpServiceImpl(config: Config)(
  implicit actorSystem: ActorSystem,
  materializer: Materializer,
  ec: ExecutionContext)
  extends HttpService
    with OrderJsonSupport
    with StrictLogging {

  override def getPhones(ids: List[Int]): Future[List[Phone]] = {
    val baseUrl = config.getString("phone.uri")
    val phoneReq = s"$baseUrl?id=${ids.mkString("&id=")}"
    logger.debug(s"Requesting phone service $phoneReq")

    val responseFuture = Http().singleRequest(HttpRequest(uri = phoneReq))

    responseFuture.flatMap { httpResp =>
      toPhoneModel(httpResp).map(_.items.toList)
    }
  }

  private def toPhoneModel(resp: HttpResponse): Future[Phones] = {
    resp.entity.dataBytes
      .map(_.utf8String)
      .map(_.parseJson.convertTo[Phones])
      .toMat(Sink.head)(Keep.right)
      .run()
  }

}
