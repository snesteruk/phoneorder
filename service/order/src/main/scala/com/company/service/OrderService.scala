package com.company.service

import com.company.model.{Order, OrderResult}
import com.company.service.OrderServiceImpl._
import com.company.util.OrderJsonSupport
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.{ExecutionContext, Future}

trait OrderService {
  def createOrder(order: Order): Future[Either[ValidationError, OrderResult]]
}

class OrderServiceImpl(http: HttpService)(implicit ec: ExecutionContext)
  extends OrderService
    with StrictLogging
    with OrderJsonSupport {

  override def createOrder(order: Order): Future[Either[ValidationError, OrderResult]] = {
    isValidOrder(order).map {
      case Valid =>
        val sum = order.phones.map(_.price).sum
        Right(OrderResult(order, sum))
      case Invalid(x) => Left(x)
    }
  }

  private def isValidOrder(order: Order): Future[ValidationResult] = {
    if (order.phones.isEmpty) Future.successful(Invalid(EmptyOrder))
    else {
      http.getPhones(order.phones.map(_.id)).map { truePhones =>
        val validPhones = order.phones.forall(truePhones.contains)
        if (validPhones) Valid else Invalid(InvalidPhone)
      }
    }
  }

}

object OrderServiceImpl {

  sealed trait ValidationError
  case object InvalidPhone extends ValidationError
  case object EmptyOrder extends ValidationError

  sealed trait ValidationResult
  case object Valid extends ValidationResult
  final case class Invalid(reason: ValidationError) extends ValidationResult

}
