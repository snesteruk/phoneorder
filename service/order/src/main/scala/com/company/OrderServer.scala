package com.company

//#quick-start-server
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.company.route.OrderRoutes
import com.company.service.{HttpServiceImpl, OrderServiceImpl}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.Duration

object OrderServer extends App with OrderRoutes with StrictLogging {

  implicit val system: ActorSystem = ActorSystem("OrderAS")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec = ExecutionContext.global

  val conf = ConfigFactory.load()
  val httpService = new HttpServiceImpl(conf)
  val orderService = new OrderServiceImpl(httpService)

  lazy val routes: Route = orderRoutes

  val binding = Http().bindAndHandle(routes, "0.0.0.0", 8081)

  logger.info(s"Server online at http://localhost:8081/")

  Await.result(system.whenTerminated, Duration.Inf)
}
