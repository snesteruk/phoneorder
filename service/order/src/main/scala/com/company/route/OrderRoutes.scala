package com.company.route

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.company.model.Order
import com.company.service.OrderService
import com.company.util.OrderJsonSupport
import com.typesafe.scalalogging.StrictLogging

import scala.util.{Failure, Success}

trait OrderRoutes extends OrderJsonSupport with StrictLogging {

  implicit def system: ActorSystem

  def orderService: OrderService

  lazy val orderRoutes: Route =
    path("order") {
      post {
        entity(as[Order]) { order =>
          logger.debug(s"received order $order")
          val res = orderService.createOrder(order)
          onComplete(res) {
            case Success(Right(orderResult)) =>
              logger.info(s"Order completed $orderResult")
              complete(orderResult)
            case Success(Left(x)) =>
              logger.warn(s"invalid order request $order validation result: $x")
              complete(StatusCodes.BadRequest)
            case Failure(ex) =>
              logger.error("Something went wrong", ex)
              complete(StatusCodes.InternalServerError)

          }
        }
      }
    }

}
